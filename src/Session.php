<?php


namespace Ucc;


final class Session
{
    public static function start()
    {
        if (isset($_SERVER['HTTP_SESSION_ID'])) {
            session_id($_SERVER['HTTP_SESSION_ID']);
        }
        session_start();
    }

    public static function destroy()
    {
        session_unset();
        session_destroy();
    }

    public static function set(string $key, string $value)
    {
        $_SESSION[$key] = $value;
    }

    public static function get(string $key): ?string
    {
        return $_SESSION[$key] ?? null;
    }

    public static function has(string $key): bool
    {
        return array_key_exists($key, $_SESSION); // perhaps semantically better than checking if the value is null on self::get()
    }
}
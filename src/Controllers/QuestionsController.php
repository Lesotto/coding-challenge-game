<?php

namespace Ucc\Controllers;

use Ucc\Http\JsonResponseTrait;
use Ucc\Models\Game;
use Ucc\Services\GameService;
use Ucc\Services\QuestionService;
use Ucc\Session;

class QuestionsController extends Controller
{
    use JsonResponseTrait;

    private QuestionService $questionService;

    public function __construct(QuestionService $questionService)
    {
        parent::__construct();
        $this->questionService = $questionService;
    }

    public function beginGame(QuestionService $questionService, GameService $gameService): bool
    {
        $name = $this->requestBody->name ?? null;

        if (empty($name)) {
            return $this->json('You must provide a name', 400);
        }

        $game = $gameService->startGame($name, $questionService->getRandomQuestion());

        return $this->json(['question' => $game->getQuestion()]);
    }

    public function answerQuestion(QuestionService $questionService, GameService $gameService): bool
    {
        $game = $gameService->getCurrentGame();
        if (!($game instanceof Game)) return $this->json('You must first begin a game', 400);

        $answer = $this->requestBody->answer ?? null;
        if (empty($answer)) {
            return $this->json('You must provide an answer', 400);
        }

        if ($game->getCorrectAnswer() === $answer) {
            $message = "Your answer was correct you've earned {$game->getQuestion()->getPoints()} points!";
            $game->increasePoints($game->getQuestion()->getPoints());
        } else {
            $message = "Your answer was not correct :(";
        }

        $game
            ->insertPlayedQuestionId($game->getQuestion()->getId())
            ->updateQuestion($questionService->getRandomQuestion(...$game->getPlayedQuestionIds()));

        if ($game->getPlayedQuestionCount() > 4) {
            Session::destroy();
            return $this->json(['message' => "Thank you for playing {$game->getName()}. Your total score was: {$game->getPoints()} points!"]);
        }

        $gameService->saveGame($game);

        return $this->json(['message' => $message, 'question' => $game->getQuestion()]);
    }
}
<?php

namespace Ucc\Models;

use JsonSerializable;

class Game implements JsonSerializable
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $points;

    /**
     * @var Question
     */
    private $question;

    /**
     * @var string
     */
    private $correctAnswer;

    /**
     * @var array
     */
    private $playedQuestionIds = [];

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Game
     */
    public function setName(string $name): Game
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getPoints(): int
    {
        return $this->points;
    }

    /**
     * @param int $points
     * @return Game
     */
    public function setPoints(int $points): Game
    {
        $this->points = $points;
        return $this;
    }

    /**
     * @param int $amount
     * @return Game
     */
    public function increasePoints(int $amount): Game
    {
        $this->points += $amount;
        return $this;
    }

    /**
     * @return Question
     */
    public function getQuestion(): Question
    {
        return $this->question;
    }

    /**
     * @param Question $question
     * @return Game
     */
    public function setQuestion(Question $question): Game
    {
        $this->question = $question;
        return $this;
    }

    /**
     * @param Question $question
     * @return Game
     */
    public function updateQuestion(Question $question): Game
    {
        $this->question = $question;
        $this->correctAnswer = $question->getCorrectAnswer();
        return $this;
    }

    /**
     * @return string
     */
    public function getCorrectAnswer(): string
    {
        return $this->correctAnswer;
    }

    /**
     * @param string $correctAnswer
     * @return Game
     */
    public function setCorrectAnswer(string $correctAnswer): Game
    {
        $this->correctAnswer = $correctAnswer;
        return $this;
    }

    /**
     * @return array
     */
    public function getPlayedQuestionIds(): array
    {
        return $this->playedQuestionIds;
    }

    /**
     * @param array $playedQuestionIds
     * @return Game
     */
    public function setPlayedQuestionIds(array $playedQuestionIds): Game
    {
        $this->playedQuestionIds = $playedQuestionIds;
        return $this;
    }

    /**
     * @param int $id
     * @return Game
     */
    public function insertPlayedQuestionId(int $id): Game
    {
        $this->playedQuestionIds[] = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getPlayedQuestionCount(): int
    {
        return count($this->playedQuestionIds);
    }

    public function jsonSerialize()
    {
        return [
            'name' => $this->name,
            'points' => $this->points,
            'correctAnswer' => $this->correctAnswer,
            'playedQuestionIds' => $this->playedQuestionIds
        ];
    }
}
<?php

namespace Ucc\Factories;

use Ucc\Models\Game;

class GameFactory
{
    /**
     * @param string $name
     * @return Game
     */
    public function makeNew(string $name)
    {
        return (new Game())
            ->setName($name)
            ->setPoints(0);
    }
}
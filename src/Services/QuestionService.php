<?php

namespace Ucc\Services;

use JsonMapper;
use KHerGe\JSON\JSON;
use Ucc\Models\Question;

class QuestionService
{
    const QUESTIONS_PATH = __DIR__ . '/../../questions.json';

    private JSON $json;
    private JsonMapper $jsonMapper;

    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $this->json = $json;
        $this->jsonMapper = $jsonMapper;
    }

    /**
     * @return Question
     */
    public function getRandomQuestion(int ...$excludeIds)
    {
        $questions = $this->getQuestions();

        $questions = array_filter($questions, function ($question) use ($excludeIds) {
            /** @var Question $question */
            return !in_array($question->getId(), $excludeIds);
        });

        return $questions[array_rand($questions)];
    }

    /**
     * @return array|object[]
     * @throws \JsonMapper_Exception
     * @throws \KHerGe\JSON\Exception\DecodeException
     * @throws \KHerGe\JSON\Exception\UnknownException
     */
    private function getQuestions()
    {
        $decodedData = $this->json->decodeFile(self::QUESTIONS_PATH);

        return array_map(function ($questionData) {
            return $this->jsonMapper->map($questionData, new Question());
        }, $decodedData);
    }
}
<?php

namespace Ucc\Services;

use Ucc\Factories\GameFactory;
use Ucc\Models\Game;
use Ucc\Models\Question;
use Ucc\Session;
use JsonMapper;

class GameService
{
    private GameFactory $factory;
    private JsonMapper $jsonMapper;

    public function __construct(GameFactory $factory, JsonMapper $jsonMapper)
    {
        $this->factory = $factory;
        $this->jsonMapper = $jsonMapper;
    }

    /**
     * @param string $name
     * @param Question $firstQuestion
     * @return \Ucc\Models\Game
     */
    public function startGame(string $name, Question $firstQuestion)
    {
        $newGame = ($this->factory->makeNew($name))
            ->setQuestion($firstQuestion)
            ->setCorrectAnswer($firstQuestion->getCorrectAnswer());

        $this->saveGame($newGame);

        return $newGame;
    }

    /**
     * @return Game|null
     * @throws \JsonMapper_Exception
     */
    public function getCurrentGame()
    {
        if (!Session::has('game')) return null;

        /** @var Game $game */
        $game = $this->jsonMapper->map(json_decode(Session::get('game')), new Game());
        $game
            ->setQuestion($this->jsonMapper->map(json_decode(Session::get('question')), new Question()))
            ->getQuestion()
                ->setCorrectAnswer($game->getCorrectAnswer());

        return $game;
    }

    /**
     * @param Game $game
     */
    public function saveGame(Game $game): void
    {
        Session::set('game', json_encode($game));
        Session::set('question', json_encode($game->getQuestion()));
    }
}